import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-factures',
  templateUrl: './factures.page.html',
  styleUrls: ['./factures.page.scss'], 
})
export class FacturesPage implements OnInit {
    public userId: any;
    public factures: any;

  constructor( 
        public afAuth: AngularFireAuth,
        public afDB: AngularFireDatabase,
        public afSG: AngularFireStorage 
      ) {
          this.afAuth.authState.subscribe(auth => {
              if (!auth) {
                console.log('non connecté');
              } else {
                console.log('Connecté: ' + auth.uid);
                this.userId = auth.uid;
                this.getFactures(); 
              }
            });
      }

  ngOnInit() {
  }
  
  getFactures() { 
      this.afDB.list('Users/' + this.userId + '/Factures').snapshotChanges() 
      .subscribe(actions => {
        const data = []; 
        actions.forEach((action: any) => {
          //this.afSG.ref(action.payload.exportVal().factureUrl).getDownloadURL().subscribe(factureUrl => {
            data.push({
              factureKey: action.key,
              factureName: action.payload.val().factureName, 
              factureUrl: action.payload.val().factureUrl,  
            });
          //});
          this.factures = data;
        });
      });
    }
    

}
