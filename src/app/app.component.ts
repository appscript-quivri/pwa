import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
      private router: Router,
    public afAuth: AngularFireAuth,  
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }
  
  initializeApp() {
      this.platform.ready().then(() => {
        this.afAuth.authState.subscribe(auth => {
          if (!auth) {
            //this.router.navigateByUrl('/'); 
            console.log('non connecté');
          } else {
            console.log('Connecté: ' + auth.uid);
            //this.router.navigateByUrl('/'); 
          }
        });
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      });
    }
    
    logOut() { 
        this.afAuth.signOut();
        this.router.navigateByUrl('/login');
    }

}
