import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AlertController } from '@ionic/angular'; 

@Component({
  selector: 'app-operation',
  templateUrl: './operation.page.html',
  styleUrls: ['./operation.page.scss'],
})
export class OperationPage implements OnInit {
    public userId: any;
    public operations: any;
    

  constructor(public afAuth: AngularFireAuth, public afDB: AngularFireDatabase, public alertController: AlertController) {
            this.afAuth.authState.subscribe(auth => {
          if (!auth) {
            console.log('non connecté');
          } else {
            console.log('Connecté: ' + auth.uid);
            this.userId = auth.uid;
            this.getOperations();
            //this.getFormData();
          }
        });
        }

  ngOnInit() {
  }
  
  getOperations() {
      this.afDB.list('Users/' + this.userId + '/Operation/').snapshotChanges() 
      .subscribe(actions => {
        const data = [];  
        actions.forEach((action: any) => {
          //this.afSG.ref(action.payload.exportVal().factureUrl).getDownloadURL().subscribe(factureUrl => {
            data.push({
              operationKey: action.key,  
                opx: action.payload.exportVal().opx,
                montant: action.payload.exportVal().montant,
                ordre: action.payload.exportVal().ordre,
                objet: action.payload.exportVal().objet, 
            });
          //});
          this.operations = data;
        });
      });
  }
  
    

}
