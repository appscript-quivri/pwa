import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MonsuivifinancierPage } from './monsuivifinancier.page';

describe('MonsuivifinancierPage', () => {
  let component: MonsuivifinancierPage;
  let fixture: ComponentFixture<MonsuivifinancierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonsuivifinancierPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MonsuivifinancierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
