import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonsuivifinancierPage } from './monsuivifinancier.page';

const routes: Routes = [
  {
    path: '',
    component: MonsuivifinancierPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonsuivifinancierPageRoutingModule {}
