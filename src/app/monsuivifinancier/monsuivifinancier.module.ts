import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonsuivifinancierPageRoutingModule } from './monsuivifinancier-routing.module';

import { MonsuivifinancierPage } from './monsuivifinancier.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MonsuivifinancierPageRoutingModule
  ],
  declarations: [MonsuivifinancierPage]
})
export class MonsuivifinancierPageModule {}
