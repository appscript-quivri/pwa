import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {
    public userId: any;
    public files: any;

  constructor(
      public afAuth: AngularFireAuth,
        public afDB: AngularFireDatabase,
        public afSG: AngularFireStorage 
      ) {
          this.afAuth.authState.subscribe(auth => {
              if (!auth) { 
                console.log('non connecté');
              } else {
                console.log('Connecté: ' + auth.uid);
                this.userId = auth.uid;
                this.getFiles(); 
              }
            });
      }

  ngOnInit() {
  }
  
  getFiles() { 
      this.afDB.list('Users/' + this.userId + '/Files').snapshotChanges() 
      .subscribe(actions => {
        const data = []; 
        actions.forEach((action: any) => {
          //this.afSG.ref(action.payload.exportVal().factureUrl).getDownloadURL().subscribe(factureUrl => {
            data.push({
              fileKey: action.key,
              fileName: action.payload.val().fileName, 
              fileUrl: action.payload.val().fileUrl,  
            });
          //});
          this.files = data;
        });
      });
    }

}
