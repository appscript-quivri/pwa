import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
      private router: Router,
    public afAuth: AngularFireAuth,
      ) {}
  
  openPage() {
        this.afAuth.authState.subscribe(auth => {
          if (!auth) {
            this.router.navigateByUrl('/login'); 
            console.log('non connecté');
          } else {
            console.log('Connecté: ' + auth.uid);
            this.router.navigateByUrl('/espace-client');  
          }
    });
  }

}
