import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AlertController } from '@ionic/angular'; 

import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.page.html',
  styleUrls: ['./formulaire.page.scss'],
})
export class FormulairePage implements OnInit {
    public userId: any;
    public operationKey: any;

    formData = { 
        operation: '',
        montant: '',
        ordre: '',
        objet: '' 
    };
    

  constructor(private router: Router, private route: ActivatedRoute, public afAuth: AngularFireAuth, public afDB: AngularFireDatabase, public alertController: AlertController) {
            this.afAuth.authState.subscribe(auth => {
          if (!auth) {
            console.log('non connecté');
          } else {
            console.log('Connecté: ' + auth.uid);
            this.userId = auth.uid;
            this.operationKey = this.route.snapshot.paramMap.get('id'); 
            this.getFormData();
          }
        });
        }

  ngOnInit() {
  } 
  getFormData() {
      this.afDB.object('Users/' + this.userId + '/' + 'Operation/' + this.operationKey).snapshotChanges() 
      .subscribe(actions => {
        //this.afSG.ref(actions.payload.exportVal().imgUrl).getDownloadURL().subscribe(url => {
          this.formData = {   
            operation: actions.payload.exportVal().opx,
            montant: actions.payload.exportVal().montant,
            ordre: actions.payload.exportVal().ordre,
            objet: actions.payload.exportVal().objet, 
          };
        //});
      });
    }
    
  
  updateForm() {
       this.afDB.object('Users/' + this.userId + '/' + 'Operation/' + this.operationKey).set({  
          montant: this.formData.montant,
          opx: this.formData.operation,  
          ordre: this.formData.ordre,
          objet: this.formData.objet 
       });  
       this.presentAlertConfirm(); 
       this.router.navigateByUrl('/operation');  
    }
    
   async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: 'Vos informations ont bien été mises à jour', 
      buttons: [
          {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
    
    

}
